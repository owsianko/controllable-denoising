# Controllable Image Denoising

![Pipeline diagram](figures/pipeline.png)

Source code for the Computational Photography course project _"Controllable Image Denoising"_ (CCID).

Authors:
* **Haley Owsianko**
* **Florian Cassayre**
* **Qiyuan Liang**

Supervisor:
* **Majed El Helou**

> **Abstract:** *Image denoising is a classic restoration problem. Yet, current deep learning methods are subject to the problems of generalization and interpretability. To mitigate these problems, in this project, we present a framework that is capable of controllable, confidence-based noise removal. The framework is based on the fusion between two different denoised images, both derived from the same noisy input. One of the two is denoised using generic algorithms (e.g. Gaussian), which make few assumptions on the input images, therefore, generalize in all scenarios. The other is denoised using deep learning, performing well on seen datasets. We introduce a set of techniques to fuse the two components smoothly in the frequency domain. Beyond that, we estimate the confidence of a deep learning denoiser to allow users to interpret the output, and provide a fusion strategy that safeguards them against out-of-distribution inputs. Through experiments, we demonstrate the effectiveness of the proposed framework in different use cases.*

## Installation

A Python 3 installation is required.
A CUDA compatible GPU is not needed but strongly recommended for the training phase.

The following command will install all the dependencies:

```
pip3 install -r requirements.txt
```

We suggest that the installation should be performed in a [virtual environment](https://docs.python.org/3/tutorial/venv.html).

## Execution

There are two executable entry points.

### GUI

The first one is the core use case of our project and will open a user interface:

```
python3 -m controllable_denoising.pipeline
```

**Important**: for the GUI to work, you might need to install the `tkinter` module if it is not already present.

Users can use the left and right arrows to switch the selected images.

### No-GUI

The other is entirely command line based is used to generate report data:

```
python3 -m controllable_denoising.pipeline_no_gui
```

The list of arguments can be retrieved with the `--help` flag.

## Structure of the implementation

### Data Flow

The data flow is shown on the above figure and described in details in the report.

1. The noisy image is first denoised using both deep learning method and generic algorithms
2. The deep learning denoised image is fused with the reliable component (from generic algorithms) of the noisy image
3. The deep learning denoised image (together with some auxiliary information) is fed to the confidence prediction model. The output is the confidence of the deep learning method.
4. The final output is the fused result, with the presence of confidence map. Optionally, users can use the confidence map to guide the fusion.

### Denoiser

`controllable_denoising/denoiser`

Responsible to produce a denoised version of an image, given an algorithm.

We divide the denoiser into two categories. 
1. The first category is the deep learning based, for example, the DnCNN, and we use DnCNN training for sigma=25 for the majority part of our experiments.
2. The second category is the generic algorithm, including Gaussian, bilateral, bicubic, bilinear (for super resolution).

Furthermore, we provide adapter code for the following reliable denoisers:

|  | Parameters |
|:---:|:---:|
| **Gaussian** | `sigma` the standard deviation of the underlying gaussian function |
| **Bilateral** | `diameter`, `sigma_color`, `sigma_space` |
| **Non-local means** | `h`, `template_window`, `search_window` |

And similarly for super-resolution:

|  | Parameters |
|:---:|:---:|
| **Bilinear** | N.A. |
| **Bicubic** | N.A. |

### Fusion

`controllable_denoising/fusion`

Fuses the denoised image with the reliable component of the noisy image, using some user-provided weight and (optionally) the estimated confidence map.

We provide different fusion strategies.

|  | Remarks |
|:---:|:---:|
| **Normal DCT** | Performs an interpolation in the DCT domain using a disk mask following a normal distribution, centered at the lowest frequencies |
| **Normal DWT** | Transforms the two images in the DWT domain and fuses iteratively |
| **Confidence DWT** | Uses the provided confidence map, patch-wise, to perform a hierarchical fusion in the DWT domain |
| **Spatial weighted** | Performs a simple linear interpolation in the spatial domain |
| **Exponential DCT** | - |
| **Primitive radius** | - |
| **L2 DCT** | - |
| **Threshold DCT** | - |

The executable file `fusion.py` serves as benchmark to assess the different fusions.

### Confidence

`controllable_denoising/confidence`

Holds the different component involved in the confidence prediction.

* `data_generation.py` is responsible to generate the data patches used in the training process
* `confidence_train.py` contains the training logic, which can optionally be run on the GPU
* `confidence.py` provides the high-level confidence prediction API: the prediction is performed given the noisy image and a denoised version, the result is a low resolution confidence map

In addition, the directory `model` contains the precise architectures definitions while the directory `saved_model` holds the latest trained models.

## Results

The raw result files can be found under `controllable_denoising/report_data`.

## Internal tools

* [Git Repository](https://gitlab.epfl.ch/owsianko/controllable-denoising)
* [Google Drive folder](https://drive.google.com/drive/u/1/folders/1L-2C_YY-uXroP59IvVCpdtz6TQguiKTZ)
* [Overleaf](https://www.overleaf.com/read/zjtdrzsfngkw)
