from skimage.io import imread, imsave
from skimage.metrics import peak_signal_noise_ratio, structural_similarity
import bm3d
import re
import numpy as np
import os

noisy_img_directory = "../dataset/noisy_data"
clean_img_directory = "../dataset/clean_data"
sub_folders = ["Test/Set12", "Test/Set68"]
denoised_save_path = "denoised_images"


def check_folder():
    for sub_folder in sub_folders:
        sub_path = os.path.join(denoised_save_path, sub_folder)
        if not os.path.exists(sub_path):
            os.makedirs(sub_path)


def denoise():
    for sub_folder in sub_folders:
        psnr_cumulative = 0
        ssim_cumulative = 0
        cnt = 0
        for im in os.listdir(os.path.join(noisy_img_directory, sub_folder)):
            if im.endswith(".jpg") or im.endswith(".bmp") or im.endswith(".png"):
                noisy_img = np.array(imread(os.path.join(noisy_img_directory, sub_folder, im)),
                                     dtype=np.float64) / 255.0
                file_name = re.match("(.*)_sigma.*", im).group(1)
                file_type = re.match(".*\.(.*)", im).group(1)
                sigma = int(re.match(".*sigma(\d+).png", im).group(1)) / 255.0
                bm3d_denoised = bm3d.bm3d(noisy_img, sigma_psd=sigma, stage_arg=bm3d.BM3DStages.ALL_STAGES)
                imsave(os.path.join(denoised_save_path, sub_folder, im),
                       (np.clip(bm3d_denoised, 0, 1) * 255).astype(np.uint8))

                clean_img = np.array(imread(os.path.join(clean_img_directory, sub_folder, file_name + "." + file_type)),
                                     dtype=np.float64) / 255.0
                psnr_denoised = peak_signal_noise_ratio(clean_img, bm3d_denoised)
                ssim_denoised = structural_similarity(clean_img, bm3d_denoised)
                psnr_cumulative += psnr_denoised
                ssim_cumulative += ssim_denoised
                cnt += 1
        print(cnt)
        print("Average PSNR: {}".format(psnr_cumulative / cnt))
        print("Average SSIM: {}".format(ssim_cumulative / cnt))


if __name__ == '__main__':
    check_folder()
    denoise()
